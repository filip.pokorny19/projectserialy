<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Class User
 * @package App\Model
 * @property int $id {primary}
 * @property string $loginName
 * @property ManyHasMany|Serialy[] $serialy {m:m Serialy::$users, isMain=true}
 * @property HasMany|Blog[] $blogs {1:m Blog::$author}
 * @property HasMany|Blogcomment[] $blogcomments {1:m Blogcomment::$author}
 */

class User extends Entity
{

}