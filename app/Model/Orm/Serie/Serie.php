<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Serie
 * @package App\Model
 * @property int $id {primary}
 * @property int $serieNumber
 * @property HasOne|Serialy $serialy {m:1 Serialy::$serie}
 * @property HasMany|Part[] $parts {1:m Part::$serie}
 */

class Serie extends Entity
{

}