<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class SeriesRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Serie::class];
    }
}
