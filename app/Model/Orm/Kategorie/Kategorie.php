<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Class Kategorie
 * @package App\Model
 * @property int $id {primary}
 * @property string $name
 * @property ManyHasMany|Serialy[] $serialy {m:m Serialy::$kategorie}
 */

class Kategorie extends Entity
{

}
