<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class KategorieRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Kategorie::class];
    }
}

