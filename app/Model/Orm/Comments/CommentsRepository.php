<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class CommentsRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Comment::class];
    }
}

