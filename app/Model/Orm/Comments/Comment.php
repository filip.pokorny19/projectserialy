<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Class Comment
 * @package App\Model
 * @property int $id {primary}
 * @property string $content
 * @property string $author
 * @property string $email
 * @property HasOne|Serialy $serialy {m:1 Serialy::$comments}
 * @property \DateTimeImmutable $createdAt {default now}
 */

class Comment extends Entity
{

}
