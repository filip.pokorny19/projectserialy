<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Part
 * @package App\Model
 * @property int $id {primary}
 * @property string $name
 * @property int $lenght
 * @property HasOne|Serialy $serialy {m:1 Serialy::$part}
 * @property HasOne|Serie $serie {m:1 Serie::$parts}
 */

class Part extends Entity
{

}
