<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class PartsRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Part::class];
    }
}

