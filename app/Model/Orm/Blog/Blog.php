<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Class Blog
 * @package App\Model
 * @property int $id {primary}
 * @property string $content
 * @property HasOne|User $author {m:1 User::$blogs}
 * @property string $title
 * @property string $image
 * @property \DateTimeImmutable $createdAt {default now}
 * @property HasMany|Blogcomment[] $blogcomments {1:m Blogcomment::$blog}
 */

class Blog extends Entity
{

}
