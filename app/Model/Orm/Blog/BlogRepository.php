<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class BlogRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Blog::class];
    }
}

