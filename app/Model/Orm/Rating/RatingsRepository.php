<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class RatingsRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Rating::class];
    }
}

