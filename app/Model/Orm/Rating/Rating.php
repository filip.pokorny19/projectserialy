<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Class Rating
 * @package App\Model
 * @property int $id {primary}
 * @property string $value
 * @property HasOne|Serialy $serialy {m:1 Serialy::$ratings}
 */

class Rating extends Entity
{

}
