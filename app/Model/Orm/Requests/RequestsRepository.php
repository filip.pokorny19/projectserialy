<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class RequestsRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Request::class];
    }
}
