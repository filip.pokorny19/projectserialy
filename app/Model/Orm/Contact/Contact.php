<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;

/**
 * Class Contact
 * @package App\Model
 * @property int $id {primary}
 * @property string $name
 * @property string $email
 * @property string $message
 */

class Contact extends Entity
{

}
