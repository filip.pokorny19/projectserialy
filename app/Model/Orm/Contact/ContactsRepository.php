<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class ContactsRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Contact::class];
    }
}

