<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Blogcomment
 * @package App\Model
 * @property int $id {primary}
 * @property string $content
 * @property string $email
 * @property HasOne|User $author {m:1 User::$blogcomments}
 * @property \DateTimeImmutable $createdAt {default now}
 * @property HasOne|Blog $blog {m:1 Blog::$blogcomments}
 */

class Blogcomment extends Entity
{

}
