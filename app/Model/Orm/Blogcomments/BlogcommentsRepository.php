<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class BlogcommentsRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Blogcomment::class];
    }
}

