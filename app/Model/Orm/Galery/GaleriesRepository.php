<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class GaleriesRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Galery::class];
    }
}

