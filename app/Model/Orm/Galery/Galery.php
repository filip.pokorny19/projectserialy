<?php
namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Galery
 * @package App\Model
 * @property int $id {primary}
 * @property string $path
 * @property HasOne|Serialy $serial {m:1 Serialy::$galery}
 */

class Galery extends Entity
{

}
