<?php
namespace App\Model;
use Nextras\Orm\Model\Model;
/**
 * Model
 * @property-read UsersRepository $users
 * @property-read SerialyRepository $serialy
 * @property-read KategorieRepository $kategorie
 * @property-read CommentsRepository $comment
 * @property-read RatingsRepository $ratings
 * @property-read SeriesRepository $serie
 * @property-read BlogRepository $blog
 * @property-read ContactsRepository $contact
 * @property-read BlogcommentsRepository $blogcomment
 * @property-read GaleriesRepository $galery
 * @property-read PartsRepository $part
 * @property-read RequestsRepository $request
 */
class Orm extends Model
{
}