<?php
namespace App\Model;

use LoveBook\Model\MessageUnread;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Class Serialy
 * @package App\Model
 * @property int $id {primary}
 * @property string $nazev
 * @property string $popis
 * @property string $delka
 * @property ManyHasMany|Kategorie[] $kategorie {m:m Kategorie::$serialy, isMain=true}
 * @property ManyHasMany|User[] $users {m:m User::$serialy}
 * @property HasMany|Comment[] $comments {1:m Comment::$serialy}
 * @property int $viewCount {default 0}
 * @property \DateTimeImmutable $createdAt {default now}
 * @property HasMany|Rating[] $ratings {1:m Rating::$serialy}
 * @property-read float $rating {virtual}
 * @property-read int $commentCount {virtual}
 * @property string $trailer
 * @property int $publicationYear
 * @property HasMany|Galery[] $galery {1:m Galery::$serial}
 * @property HasMany|Part[] $part {1:m Part::$serialy}
 * @property HasMany|Serie[] $serie {1:m Serie::$serialy}
 */

class Serialy extends Entity
{
    public function getterRating()
    {
        $rating = $this->ratings;
        $a=0;
        $avg=0;
        $b = $this->ratings->countStored();

        foreach ($rating as $item)
        {
            $a += $item->value;
        }
        //ochrana proti dělení nulou
        if ($b!=0||$a!=0)
        {
            $avg=$a/$b;
        }

        return $avg;
    }

    public function getterCommentCount()
    {
        $commentCount=$this->comments->countStored();
        return $commentCount;
    }
}
