<?php

namespace App\Presenters;

class SearchPresenter extends BasePresenter
{
    public function actionDefault(string $name)
    {
        $serialy=$this->orm->serialy->findBy(['nazev'=>$name]);
        $this->template->serialy=$serialy;
    }
}
