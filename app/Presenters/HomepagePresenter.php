<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\User;
use Nette;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Tracy\Debugger;


final class HomepagePresenter extends BasePresenter
{
    public User $databaseUser;


    /*

    public function actionDefault()
    {
        $newVideos=$this->orm->serialy->findAll()->limitBy(3)->orderBy('createdAt', 'DESC');
        $this->template->newVideos=$newVideos;
    }

    */

    public function actionFavourite()
    {
        $newVideos=$this->orm->serialy->findAll()->limitBy(5)->orderBy('viewCount', 'DESC');
        $this->template->newVideos=$newVideos;
    }

    public function handleViewCounter($id)
    {
        $serial=$this->orm->serialy->getById($id);
        $serial->viewCount=$serial->viewCount+1;
        $this->orm->persistAndFlush($serial);
        $this->redirect('Serial:detail',[$id]);
    }




}
