<?php

namespace App\Presenters;


use Nette\Application\UI\Form;
use Nette\ComponentModel\IComponent;
use Tracy\Debugger;

class LadderPresenter extends BasePresenter
{

    public $actualOrderBy = 'default';
    public $actualCategory= 'defaultCategory';
    public $actualYear = 'defaultYear';

    public function actionDefault($orderBy = null, $categoryId=null, $publicationYear=null)
    {
        if ($categoryId !== null) {
            $favouriteSerials = $this->orm->serialy->findBy(['this->kategorie->id' => $categoryId]);
        }
        else {
            $favouriteSerials = $this->orm->serialy->findAll();
        }

        if ($orderBy && $orderBy !== 'default')
        {
            if ($orderBy=='publicationYear') {
                $favouriteSerials = $favouriteSerials->orderBy('publicationYear', 'DESC');
            }elseif ($orderBy=='old')
            {
                $favouriteSerials = $favouriteSerials->orderBy('publicationYear', 'ASC');
            }
        }




        $this->actualYear = $publicationYear;
        $this->actualOrderBy = $orderBy;
        $this->actualCategory=$categoryId;
        $this->template->favouriteSerials=$favouriteSerials;
    }

    public function createComponentOrderForm(string $name): ?IComponent
    {


        $form = new Form();
        $form->addSelect('orderBy', 'řadit dle', [
            'default' => 'Výchozí',
            'viewCount' => 'Shlédnutí',
            'publicationYear' => 'Nejnovější',
            'old'=>'Nejstarší'
        ])->setDefaultValue($this->actualOrderBy);
        $form->addSelect('category', 'kategorie',
            $this->orm->kategorie->findAll()->fetchPairs('id', 'name'))->setPrompt('Vše')->setDefaultValue($this->actualCategory);
        $form->addSelect('publication', 'Rok vydání',
            $this->orm->serialy->findAll()->orderBy('publicationYear','DESC')->fetchPairs(null, 'publicationYear'))->setPrompt('Vše')->setDefaultValue($this->actualYear);
        $form->addSubmit('submit','řadit');
        $form->onSuccess[] = [$this, 'orderFormSubmit'];

        return $form;
    }


    public function orderFormSubmit(Form  $form)
    {
        $this->redirect('this', [
            'orderBy' => $form->getValues()['orderBy'],
            'categoryId' => $form->getValues()['category'],
            'publicationYear' => $form->getValues()['publication']
        ]);
    }
}
