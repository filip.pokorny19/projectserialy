<?php

namespace App\Presenters;

use App\Model\Orm;
use App\Model\User;
use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ReviewPresenter extends Nette\Application\UI\Presenter

{
    /** @inject */
    public Orm $orm;
    public function createComponentReviewForm(): Form
    {
        $form = new Form;

        $form ->addText('revName', 'Jméno')
            ->setRequired('Zadejte prosím jméno');

        $form ->addText('revSecName', 'Příjmení:');

        $form ->addEmail('revMail', 'Email:')
            ->setRequired('Zadejte prosím email');

        $form ->addText('revOrderNumber','Číslo objednávky:');

        $stars = ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5']; //pude to udělat líp
        $form->addRadioList('stars', 'Počet hvězd', $stars);

        $form ->addTextArea('revMessage','Zpráva:');

        $form ->addSubmit('send', 'Odeslat recenzi');

        $form->onSuccess[]=[$this,'reviewFormSucces'];
        return $form;


    }
    public function reviewFormSucces(Form $form)
    {
        //dodělat propojení s databází
    }


}
