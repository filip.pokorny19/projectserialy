<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use App\Model\Orm;
use Nextras\Orm\Collection\ICollection;
use Tracy\Debugger;

class BasePresenter extends Presenter {

    /** @inject */
    public Orm $orm;

    public function getCategories(): ICollection
    {
        $categories = $this->orm->kategorie->findAll();

        return $categories;
    }



}