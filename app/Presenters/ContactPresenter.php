<?php

namespace App\Presenters;

use App\Model\Contact;
use App\Model\Orm;
use App\Model\User;
use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;



class ContactPresenter extends BasePresenter
{

    public function createComponentContactForm(): Form
    {
        $form = new Form;

        $form->addText('name', 'Jméno:')
            ->setRequired('Zadejte prosím jméno');

        $form->addEmail('email', 'Email:')
            ->setRequired('Zadejte prosím email');

        $form->addTextArea('message','Zpráva:')
            ->setRequired('Napište nám zprávu:');

        $form->addSubmit('odeslat', 'Odeslat');

        $form->onSuccess[]=[$this,'contactFormSucces'];
        return $form;
    }

    public function contactFormSucces(Form $form)
    {
        $values = $form->getValues();
        $contact = new Contact();
        $contact->name=$values->name;
        $contact->email=$values->email;
        $contact->message= $values->message;

        $mail = new Nette\Mail\Message();
        $mail->setFrom('bunis@seznam.cz')
                ->addTo('filip.pokorny19@seznam.cz');
        $mail->setBody('nazdar');

        $mailer = new Nette\Mail\SmtpMailer();
        $mailer->send($mail);

        $this->orm->persistAndFlush($contact);
        $this->flashMessage('Formulář byl úspěšně odeslán.');
        $this->redirect('this');
    }

}


