<?php

namespace App\Presenters;


use App\Model\Request;
use Nette\Application\UI\Form;

class RequestPresenter extends BasePresenter
{

    public function createComponentRequestForm(): Form
    {
        $form = new Form();

        $form->addText('name', 'Název seriálu:')
            ->setRequired('Toto pole je povinné.');

        $form->addText('vote', 'Hlasy');
        //TODO dořešit s petrem, aby nebylo vidět

        $form->addSubmit('send', 'Přidat přání');
        $form->onSuccess[]=[$this, 'requestFormSucces'];
        return $form;
    }

    public function requestFormSucces(Form $form)
    {
        $values=$form->getValues();
        $request = new Request();
        $request->name=$values->name;
        $request->vote=$values->vote;
        $this->orm->persistAndFlush($request);
        $this->flashMessage('Přání bylo přidáno.');
        $this->redirect('this');

    }

    public function actionDefault()
    {
        $requestSerial = $this->orm->request->findAll();
        $this->template->requestSerial=$requestSerial;
    }

    public function handleVoteCounter($id)
    {
        $requestCounter = $this->orm->request->getById($id);
        $requestCounter->vote = $requestCounter->vote+1;
        //...
        $this->orm->persistAndFlush($requestCounter);
        $this->redirect('this');
        //TODO dodělat řazení podle počtu hlasů
    }
}
