<?php

namespace App\Presenters;

use App\Model\Orm;
use App\Model\User;
use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;



class RegistrationPresenter extends Nette\Application\UI\Presenter
{
    /** @inject  */
    public Orm $orm;
    public function createComponentRegistrationForm(): Form
    {
        $form = new Form;

        $form->addEmail('regMail', 'Email:')
            ->setRequired('Zadejte prosím email');

        $form->addText('regName', 'Nick:')
            ->setRequired('Zadejte prosím přihlašovací nick');

        $form->addPassword('regPass', 'Heslo:')
            ->setRequired('Zadejte prosím heslo');

        $form->addPassword('regPassVerify', 'Heslo znovu:')
            ->setRequired('Zadejte prosím heslo znovu pro kontrolu')
            ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['regPass']);


        $form->addSubmit('loginSend', 'Registrovat se');

        $form->onSuccess[]=[$this,'registrationFormSucces'];
        return $form;
    }



}




