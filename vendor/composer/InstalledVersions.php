<?php

namespace Composer;

use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'c0a51b036a0c54ad7bab0e79d1e4ec45a84aeb2a',
    'name' => 'nette/web-project',
  ),
  'versions' => 
  array (
    'contributte/console' => 
    array (
      'pretty_version' => 'v0.8.0',
      'version' => '0.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5bf019e2df77683a2473129f8aa6af32c8ab7c3e',
    ),
    'contributte/di' => 
    array (
      'pretty_version' => 'v0.4.2',
      'version' => '0.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ec21373c037838a61bac5a1b1833b84b9d1e7d3',
    ),
    'kdyby/strict-objects' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0579f929516d4b62a5adb93b5c21888995de3ce',
    ),
    'kdyby/translation' => 
    array (
      'pretty_version' => 'v3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '1f04a53f04a554010282cadb0d011a10bdca1472',
    ),
    'latte/latte' => 
    array (
      'pretty_version' => 'v2.8.2',
      'version' => '2.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a3fbffc89f85b2745bc058278c882f745b6ad89b',
    ),
    'nette/application' => 
    array (
      'pretty_version' => 'v3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '783ad6fc6444f63314175131885c04b3dd0291dd',
    ),
    'nette/bootstrap' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '67830a65b42abfb906f8e371512d336ebfb5da93',
    ),
    'nette/caching' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9ecbf920f240bd1ab14900d9a77876924ad7fb4',
    ),
    'nette/component-model' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '66409cf5507c77edb46ffa88cf6a92ff58395601',
    ),
    'nette/database' => 
    array (
      'pretty_version' => 'v3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'daccbd526f74311549e5c81d3181fc74f87c6733',
    ),
    'nette/di' => 
    array (
      'pretty_version' => 'v3.0.5',
      'version' => '3.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '766e8185196a97ded4f9128db6d79a3a124b7eb6',
    ),
    'nette/finder' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ad2c298eb8c687dd0e74ae84206a4186eeaed50',
    ),
    'nette/forms' => 
    array (
      'pretty_version' => 'v3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ecb5f7b8c82585c5fc4698ccb6815542fe6b2db4',
    ),
    'nette/http' => 
    array (
      'pretty_version' => 'v3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4d8d360c66c8af9512ca13ab629d312af2b3ce3',
    ),
    'nette/mail' => 
    array (
      'pretty_version' => 'v3.1.4',
      'version' => '3.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf56ecfd338335f7873c8f356134b3a0960653f8',
    ),
    'nette/neon' => 
    array (
      'pretty_version' => 'v3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5b3a60833d2ef55283a82d0c30b45d136b29e75',
    ),
    'nette/php-generator' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7051954c534cebafd650efe8b145ac75b223cb66',
    ),
    'nette/reflection' => 
    array (
      'pretty_version' => 'v2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b12327e98ead74e87a1315e0d48182a702adf901',
    ),
    'nette/robot-loader' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '15c1ecd0e6e69e8d908dfc4cca7b14f3b850a96b',
    ),
    'nette/routing' => 
    array (
      'pretty_version' => 'v3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '603c697f3df7ed214795d4e8e8c58fbf981232b1',
    ),
    'nette/schema' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'febf71fb4052c824046f5a33f4f769a6e7fa0cb4',
    ),
    'nette/security' => 
    array (
      'pretty_version' => 'v3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '2eacedebfbeb9d9587ef1e59c18d170879474c61',
    ),
    'nette/tester' => 
    array (
      'pretty_version' => 'v2.3.3',
      'version' => '2.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fbe4860b4cfabe99f03ee3632db6352f0564d22',
    ),
    'nette/tokenizer' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '14d42330fb299e877ad5a01f955098e35190c042',
    ),
    'nette/utils' => 
    array (
      'pretty_version' => 'v3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c09937fbb24987b2a41c6022ebe84f4f1b8eec0f',
    ),
    'nette/web-project' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'c0a51b036a0c54ad7bab0e79d1e4ec45a84aeb2a',
    ),
    'nextras/dbal' => 
    array (
      'pretty_version' => 'v4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb7cac5bd7793229733fc0b997e7cf8a6acb5369',
    ),
    'nextras/migrations' => 
    array (
      'pretty_version' => 'v3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d5983de55cc51124012153477f6aa07440c935b',
    ),
    'nextras/orm' => 
    array (
      'pretty_version' => 'v4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7272f308b69bef3e3a9f16c7bed8fd180ed1efab',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e85481cf359a7b28a44ac91f7d83441b70d76192',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0b2c29c0fa6a69089209bbe8fcff4df2a313d0e',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df08650ea7aee2d925380069c131a66124d79177',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ba089a5b6366e453971d3aad5fe8e897b37f41',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7cf3f858ec7d70b89559d6e6eb1f7c2517d479c',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '727d1096295d807c309fb01a851577302394c897',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39d483bdf39be819deabf04ec872eb0b2410b531',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ff431c517be11c78c48a39a66d37431e26a6bed',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e70aa8b064c5b72d3df2abd5ab1e90464ad009de',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15da7ba4957ffb8f1747218be9e1a121fd298a1',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a97573e960303db71be0dd8fda9be3bca5e0feea',
    ),
    'symfony/thanks' => 
    array (
      'pretty_version' => 'v1.2.10',
      'version' => '1.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e9c4709560296acbd4fe9e12b8d57a925aa7eae8',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '73095716af79f610f3b6338b911357393fdd10ab',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2eaa60b558f26a4b0354e1bbb25636efaaad105',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'tracy/tracy' => 
    array (
      'pretty_version' => 'v2.7.5',
      'version' => '2.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '95b1c6e35d61df8ef91a4633ba3cf835d2d47e5e',
    ),
  ),
);







public static function getInstalledPackages()
{
return array_keys(self::$installed['versions']);
}









public static function isInstalled($packageName)
{
return isset(self::$installed['versions'][$packageName]);
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

$ranges = array();
if (isset(self::$installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = self::$installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}





public static function getVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['version'])) {
return null;
}

return self::$installed['versions'][$packageName]['version'];
}





public static function getPrettyVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return self::$installed['versions'][$packageName]['pretty_version'];
}





public static function getReference($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['reference'])) {
return null;
}

return self::$installed['versions'][$packageName]['reference'];
}





public static function getRootPackage()
{
return self::$installed['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
}
}
